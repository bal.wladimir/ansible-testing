FROM alpine:3.11

ARG USER=docker

ADD https://php.hernandev.com/key/php-alpine.rsa.pub /etc/apk/keys/php-alpine.rsa.pub

# make sure you can use HTTPS
RUN apk --update add ca-certificates && \
    echo "https://php.hernandev.com/v3.11/php-7.4" >> /etc/apk/repositories

# Install packages
RUN apk --no-cache add php php-fpm php-opcache php-openssl php-curl openrc \
    nginx=1.16.1-r8 supervisor curl
RUN apk --no-cache --update add python3 python3-dev && ln -sf python3 /usr/bin/python

# https://github.com/codecasts/php-alpine/issues/21
RUN ln -s /usr/bin/php7 /usr/bin/php

RUN python -m pip install --upgrade pip
RUN pip3 install --no-cache-dir --upgrade pip setuptools pytest-testinfra

RUN mkdir -p /etc/nginx/test
RUN mkdir -p /etc/nginx/test_report
COPY tests/test.py /etc/nginx/test

# Configure nginx
COPY config/nginx.conf /etc/nginx/nginx.conf

# Remove default server definition
RUN rm /etc/nginx/conf.d/default.conf

# Configure PHP-FPM
COPY config/fpm-pool.conf /etc/php7/php-fpm.d/www.conf
COPY config/php.ini /etc/php7/conf.d/custom.ini

# Configure supervisord
COPY config/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

# Setup document root
RUN mkdir -p /var/www/html

RUN addgroup -S $USER && adduser -S $USER -G $USER

# Make sure files/folders needed by the processes are accessable when they run under the nobody user
RUN chown -R $USER.$USER /var/www/html && \
  chown -R $USER.$USER /run && \
  chown -R $USER.$USER /var/lib/nginx && \
  chown -R $USER.$USER /var/log/nginx

# Switch to use a non-root user from here on
USER $USER

# Add application
WORKDIR /var/www/html
COPY --chown=$USER src/ /var/www/html/

# Expose the port nginx is reachable on
EXPOSE 8080

# Let supervisord start nginx & php-fpm
CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]

# Configure a healthcheck to validate that everything is up&running
HEALTHCHECK --timeout=10s CMD curl --silent --fail http://127.0.0.1:8080/fpm-ping

RUN pytest /etc/nginx/test/test.py
